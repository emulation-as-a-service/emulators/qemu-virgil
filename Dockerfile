FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base:update-18-04

LABEL "EAAS_EMULATOR_TYPE"="qemu-virgil"
LABEL "EAAS_EMULATOR_VERSION"="v3.1"

RUN apt-get update 
RUN apt-get upgrade -y
RUN lsb_release -a
RUN add-apt-repository cloud-archive:stein -y
RUN apt-get update
RUN apt-get install qemu qemu-system qemu-system-x86 -y
